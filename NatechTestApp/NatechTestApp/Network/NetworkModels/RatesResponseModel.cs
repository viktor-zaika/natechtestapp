﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace NatechTestApp.NetwokrService.NetworkModels
{
    public class RatesResponseModel
    {
        [JsonProperty("timestamp")]
        public int Timestamp;

        [JsonProperty("base")]
        public string BaseCurrency;

        [JsonProperty("rates")]
        public Dictionary<string, double> Rates;
    }
}

