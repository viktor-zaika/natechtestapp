﻿
using NatechTestApp.ViewModels;
using Xamarin.Forms;

namespace NatechTestApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel();
        }
    }
}

