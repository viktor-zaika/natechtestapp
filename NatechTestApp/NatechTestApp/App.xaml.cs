﻿using System;
using DryIoc;
using NatechTestApp.NetwokrService;
using NatechTestApp.Network;
using NatechTestApp.Services;
using Xamarin.Forms;
using Xamarin.Forms.Svg;
using Xamarin.Forms.Xaml;

namespace NatechTestApp
{
    public partial class App : Application
    {
        public static Container Container;

        public App ()
        {
            SetupDependencies();

            InitializeComponent();
            SvgImageSource.RegisterAssembly();

            MainPage = new MainPage();
        }

        protected override void OnStart ()
        {
            // Nothing to do
        }

        protected override void OnSleep ()
        {
            // Nothing to do
        }

        protected override void OnResume ()
        {
            // Nothing to do
        }

        private void SetupDependencies()
        {
            Container = new Container();

            Container.Register<BaseNetworkService>(Reuse.Singleton);
            Container.Register<IOpenExchangeNetworkService, OpenExchangeNetworkService>();
            Container.Register<ICurrencyExchangeService, CurrencyExchangeService>();
        }
    }
}

