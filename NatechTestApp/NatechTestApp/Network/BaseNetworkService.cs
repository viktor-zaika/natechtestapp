﻿using NatechTestApp.NetwokrService.NetworkModels.BaseNetworkModels;
using System.Threading.Tasks;
using RestSharp;
using Plugin.Connectivity;
using Newtonsoft.Json;

namespace NatechTestApp.Network
{
    public sealed class BaseNetworkService
    {
        private const string BaseUrl = @"https://openexchangerates.org/api/";
        private const string AppId = "b9e0663a5a0d4c269aabc1f8e92d8e6d";
        private RestClient client;

        public BaseNetworkService()
        {
            SetupClient();
        }

        private void SetupClient()
        {

            client = new RestClient(BaseUrl);
            client.AddDefaultHeader("Content-Type", "application/json");
            client.AddDefaultParameter("app_id", AppId);
            
        }

        private async Task<RequestResult<T>> ValidateNetworkResponseAsync<T>(RestResponse response)
        {
            if (response.IsSuccessful)
            {
                return RequestResult<T>.Ok(JsonConvert.DeserializeObject<T>(response.Content));
            }
            else
            {
                if (response.StatusCode == 0)
                    return RequestResult<T>.NoConnection();
                else 
                    return RequestResult<T>.Failed();
            }
        }

        public async Task<RequestResult<T>> ExecuteTaskAsync<T>(RestRequest request)
        {
            try
            {
                var response = await client.ExecuteAsync(request);

                return await ValidateNetworkResponseAsync<T>(response);
            }
            catch (TaskCanceledException)
            {
                return RequestResult<T>.NoConnection(default(T));
            }
        }
    }
}

