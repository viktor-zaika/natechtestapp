﻿using System;
using System.Threading.Tasks;
using NatechTestApp.NetwokrService.NetworkModels;
using NatechTestApp.NetwokrService.NetworkModels.BaseNetworkModels;
using NatechTestApp.Network;
using RestSharp;

namespace NatechTestApp.NetwokrService
{
    public class OpenExchangeNetworkService : IOpenExchangeNetworkService
    {
        private BaseNetworkService networkService;

        public OpenExchangeNetworkService(BaseNetworkService baseNetworkService)
        {
            networkService = baseNetworkService;
        }

        public async Task<RequestResult<RatesResponseModel>> GetRates(string baseCurrency)
        {
            var request = new RestRequest("latest.json", Method.Get);
            request.AddParameter("base", baseCurrency);

            var response = await networkService.ExecuteTaskAsync<RatesResponseModel>(request);

            return response;
        }
    }
}

