﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NatechTestApp.NetwokrService;

namespace NatechTestApp.Services
{
    public class CurrencyExchangeService : ICurrencyExchangeService
    {
        private IDictionary<string, long> providerLimits = new Dictionary<string, long>()
        {
            { "USD", 100 * 1000 },
            { "EUR", 300 * 1000 }
        };

        private DateTime lastUpdateTimeStamp;

        private double usdToEuroRate;
        private double euroToUsdRate;

        private readonly IOpenExchangeNetworkService networkService;


        public CurrencyExchangeService(IOpenExchangeNetworkService service)
        {
            networkService = service;
        }

        public async Task<double> GetExchangeForecast(string fromCurrency, string toCurrency, long amount, bool forceUpdate = false)
        {
            if (forceUpdate || lastUpdateTimeStamp == null || lastUpdateTimeStamp.AddMinutes(1) < DateTime.Now)
            {
                await UpdateRates();
            }

            var resultValue = amount * (toCurrency == "EUR" ? usdToEuroRate : euroToUsdRate);

            return resultValue;
        }

        public long GetExchangeLimit(string currency)
        {
            // Because https://openexchangerates.org/ doesn't provide endpoint to fetch this info hardcoded
            // values had been used.

            return providerLimits[currency];
        }

        private async Task<bool> UpdateRates()
        {
            var usdToOtherCurrenciesRate = await networkService.GetRates("USD");
            // Because https://openexchangerates.org/ require developer account level to
            // perform requests with changed base currency. That is why value will be hardcoded.

            //var euroToOtherCurrenciesRate = await networkService.GetRates("EUR");

            if (usdToOtherCurrenciesRate.Successfull /*&& euroToOtherCurrenciesRate.Successfull*/) {
                usdToEuroRate = usdToOtherCurrenciesRate.Result.Rates["EUR"];
                euroToUsdRate = 1.1;   //usdToOtherCurrenciesRate.Result.Rates["USD"];
                lastUpdateTimeStamp = DateTime.Now;

                return true;
            }

            return false;
        }
    }
}

