﻿using System;
using System.Threading.Tasks;

namespace NatechTestApp.Services
{
	public interface ICurrencyExchangeService
	{
		Task<double> GetExchangeForecast(string fromCurrency, string toCurrency, long amount, bool forceUpdate = false);

		long GetExchangeLimit(string currency);
	}
}

