﻿using System;
namespace NatechTestApp.NetwokrService.NetworkModels.BaseNetworkModels
{
    public class RequestResult
    {
        public bool Successfull { get; set; }

        public NetworkError Status { get; set; }

        public string Errors { get; set; }

    }

    public class RequestResult<T> : RequestResult
    {
        public T Result { get; set; }

        public static RequestResult<T> Failed(T data = default(T))
        {
            return new RequestResult<T> { Successfull = false, Status = NetworkError.UnexpectedFailure, Result = data, Errors = "Service is temporarily unavalable. Please try again later." };
        }

        public static RequestResult<T> Ok(T result)
        {
            return new RequestResult<T> { Successfull = true, Status = NetworkError.None, Result = result };
        }

        public static RequestResult<T> NoConnection(T result = default(T))
        {
            return new RequestResult<T> { Successfull = false, Status = NetworkError.ConnectivityIssue, Result = result };
        }
    }

    public enum NetworkError
    {
        None,
        UnexpectedFailure,
        ConnectivityIssue
    }
}

