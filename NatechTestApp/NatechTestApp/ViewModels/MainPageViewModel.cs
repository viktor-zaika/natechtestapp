﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DryIoc;
using NatechTestApp.Services;
using Xamarin.Forms;

namespace NatechTestApp.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        private ICurrencyExchangeService currencyExchangeService;

        private string _fromCurrency;
        public string FromCurrency
        {
            get { return _fromCurrency; }
            set { SetProperty(ref _fromCurrency, value); }
        }

        private string _toCurrency;
        public string ToCurrency
        {
            get { return _toCurrency; }
            set { SetProperty(ref _toCurrency, value); }
        }

        private string _toAmount;
        public string ToAmount
        {
            get { return _toAmount; }
            set { SetProperty(ref _toAmount, value); }
        }

        private string _fromAmount;
        public string FromAmount
        {
            get { return _fromAmount; }
            set
            {
                SetProperty(ref _fromAmount, value);
            }
        }

        private string _exchangeLimit;
        public string ExchangeLimit
        {
            get { return _exchangeLimit; }
            set { SetProperty(ref _exchangeLimit, value); }
        }

        private bool _isRateRefreshing;
        public bool IsRateRefreshing
        {
            get { return _isRateRefreshing; }
            set { SetProperty(ref _isRateRefreshing, value); }
        }

        public ICommand RefreshRateCommand { get; set; }

        public ICommand SwitchCurrencyCommand { get; set; }


        public MainPageViewModel()
        {
            currencyExchangeService = Container.Resolve<ICurrencyExchangeService>();

            SwitchCurrencyCommand = new Command(SwitchCurrency);
            RefreshRateCommand = new Command(RefreshRate);

            InitializeContent();
        }

        private void HandleArgumentsChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(ToAmount))
                UpdateForecast();
        }

        private void InitializeContent()
        {
            FromCurrency = "USD";
            ToCurrency = "EUR";
            FromAmount = "0";
            ToAmount = "0";
            ExchangeLimit = currencyExchangeService.GetExchangeLimit(ToCurrency).ToString();

            UpdateForecast();

            PropertyChanged += HandleArgumentsChanged;
        }

        private async void UpdateForecast()
        {
            if (string.IsNullOrWhiteSpace(FromAmount))
                return;

            if (long.Parse(FromAmount) > long.Parse(ExchangeLimit))
                FromAmount = ExchangeLimit;

            ToAmount = (await currencyExchangeService.GetExchangeForecast(FromCurrency, ToCurrency, long.Parse(FromAmount), true)).ToString();
        }

        private void SwitchCurrency()
        {
            FromAmount = "0";
            ToAmount = "0";

            var temp = FromCurrency;
            FromCurrency = ToCurrency;
            ToCurrency = temp;

            ExchangeLimit = currencyExchangeService.GetExchangeLimit(ToCurrency).ToString();
        }

        private async void RefreshRate()
        {
            if (string.IsNullOrWhiteSpace(FromAmount))
                return;

            IsRateRefreshing = true;

            await currencyExchangeService.GetExchangeForecast(FromCurrency, ToCurrency, long.Parse(FromAmount), true);

            IsRateRefreshing = false;
        }
    }
}

