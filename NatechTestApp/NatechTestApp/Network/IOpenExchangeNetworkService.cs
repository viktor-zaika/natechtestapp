﻿using System;
using NatechTestApp.NetwokrService.NetworkModels;
using NatechTestApp.NetwokrService.NetworkModels.BaseNetworkModels;
using System.Threading.Tasks;

namespace NatechTestApp.NetwokrService
{
    public interface IOpenExchangeNetworkService
    {
        Task<RequestResult<RatesResponseModel>> GetRates(string baseCurrency);
    }
}

